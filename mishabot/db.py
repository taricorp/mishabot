# Database URI must be conigured before initial db import
from sqlalchemy import create_engine
from mishabot import database_uri
engine = create_engine(database_uri)

# Declarative style base class
from sqlalchemy.schema import Column
from sqlalchemy.types import Boolean, DateTime, Integer, Unicode
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

# Sessions bound to connection
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)

from mishabot import scraper
from datetime import datetime

class URL(Base):
    __tablename__ = 'urls'

    id = Column(Integer, primary_key=True)
    url = Column(Unicode(length=256), nullable=False)
    sfw = Column(Boolean, default=True)
    title = Column(Unicode(length=128))

    timestamp = Column(DateTime, default=datetime.now, nullable=False)
    network = Column(Unicode(length=32))
    channel = Column(Unicode(length=32))
    nick = Column(Unicode(length=32))

    def __init__(self, url, **kwargs):
        kwargs['url'] = url
        kwargs['title'] = scraper.scrape_http(url)
        super(URL, self).__init__(**kwargs)

    def __repr__(self):
        return '<"%s" (%s)>' %(self.title, self.url)

def withSession(f):
    def g(self, *args, **kwargs):
        s = Session()
        result = f(self, s, *args, **kwargs)
        s.close()
        return result
    return g

def create_tables():
    Base.metadata.create_all(engine)
