from __future__ import print_function

from twisted.internet.protocol import ReconnectingClientFactory as ReconnClient
from twisted.words.protocols.irc import IRCClient

from mishabot import db
import re

class MishaBotNetwork(ReconnClient):
    def __init__(self, hosts, channels, nick='MishaBot', net_name=None):
        self.hosts = hosts
        self.channels = channels
        self.nick = nick
        self.net_name = net_name or hosts[0]

    def __str__(self):
        return self.net_name

    def startedConnecting(self, connector):
        print('Trying to connect to', self)

    def clientConnectionFailed(self, connector, reason):
        print('Failed to connect to {0}: {1}'.format(self,
            reason.getErrorMessage()))
        ReconnClient.clientConnectionFailed(self, connector, reason)

    def clientConnectionLost(self, connector, reason):
        print('Lost connection to {0}: {1}'.format(self,
            reason.getErrorMessage()))
        ReconnClient.clientConnectionLost(self, connector, reason)

    def buildProtocol(self, addr):
        print('Connected to', self)
        self.resetDelay()
        return MishaBot(self.nick, self.channels, self.net_name)


class MishaBot(IRCClient):
    URLPATTERN = re.compile('https?://\S+', re.IGNORECASE | re.UNICODE)

    def __init__(self, nick, channels, net_name=None):
        self.net_name = net_name

        self.nickname = nick
        self.desiredNick = nick

        self.channels = []
        self.desiredChannels = channels

    def alterCollidedNick(self, nickname):
        # TODO something fun like 1337-sp33k substitutions
        return IRCClient.alterCollidedNick(self, nickname)

    def signedOn(self):
        assert len(self.channels) == 0
        for channel in self.desiredChannels:
            self.join(channel)

    def connectionLost(self, reason):
        self.channels = []

    def joined(self, channel):
        assert channel not in self.channels
        print('Joined', channel)
        self.channels.append(channel)

    def left(self, channel):
        # TODO does this apply when kicked too? (don't think so)
        assert channel in self.channels
        self.channels.remove(channel)

    def privmsg(self, user, channel, message):
        message = message.lower()
        sfw = 'nsfw' not in message

        # Defer creating a Session until we actually need one, since many
        # messages don't have URLs in them.
        s = None
        # TODO yegads, these things can block
        for url in re.finditer(self.URLPATTERN, message):
            if not s:
                s = db.Session()
            s.add(db.URL(url.group(),
                         sfw=sfw,
                         channel=channel,
                         nick=user.partition('!')[0],
                         network=self.net_name))
        if s:
            s.commit()

        print('{0} <{1}>: {2}'.format(channel, user, message))
