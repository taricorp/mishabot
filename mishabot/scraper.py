
# Might be handy do a plugin-based sort of thing here in the future.
#from pkg_resources import working_set
# or perhaps
#pkg_resources.resource_listdir('mishabot', 'scrapers')

from twisted.python import log

from htmlentitydefs import name2codepoint
from HTMLParser import HTMLParser
from urllib2 import urlopen

def extract_charset(s):
    params = s.lower().split(';')
    for k, v in map(lambda x: x.strip().split('=', 1), params[1:]):
        if k == 'charset':
            return v
    return None

class HTMLTitleExtractor(HTMLParser):
    def __init__(self, encoding=None):
        HTMLParser.__init__(self)
        self.encoding = encoding or 'UTF-8'
        self.title = bytes()
        self.in_title = False
        self.done = False

    def get_title(self):
        return self.title.decode(self.encoding, 'replace')

    def handle_starttag(self, tag, attrs):
        if tag == 'title':
            self.in_title = True
        elif tag == 'meta':
            try:
                attrs = dict(attrs)
                if attrs['http-equiv'] == 'Content-Type':
                    # RFC 2616 section 3.7
                    charset = extract_charset(attrs['content'])
                    if charset:
                        self.encoding = charset
            except (KeyError, IndexError, ValueError):
                pass    # Not http-equiv, malformed, or useless

    def handle_data(self, data):
        if self.in_title:
            self.title += data
    def handle_entityref(self, name):
        if self.in_title:
            self.title += unichr(name2codepoint[name])
    def handle_charref(self, name):
        if self.in_title:
            if name.startswith('x'):
                c = unichr(int(name[1:], 16))
            else:
                c = unichr(int(name))
            self.title += c
    def handle_endtag(self, tag):
        if tag == 'title':
            self.in_title = False
        elif tag == 'head':
            self.done = True

def scrape_http(url):
    f = None
    try:
        f = urlopen(url, timeout=10)
        if f.info().gettype() != 'text/html':
            return None

        # Use charset from HTTP headers if available
        ctype = f.info().getheader('Content-Type')
        encoding = extract_charset(ctype) if ctype else None
        e = HTMLTitleExtractor(encoding)

        for line in f:
            e.feed(line.decode(e.encoding))
            if e.done:
                break
        else:
            return None
        return e.get_title()
    except:
        log.err()
        return None

    finally:
        if f != None:
            f.close()

if __name__ == '__main__':
    import sys
    log.startLogging(sys.stdout, setStdout=False)
    for u in sys.argv[1:]:
        print u, scrape_http(u)
