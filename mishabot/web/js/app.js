angular.module('mishabot', ['mishabot.services', 'mishabot.controllers', 'mishabot.directives']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/links', {
                templateUrl: 'templates/links-list.html',
                controller: 'LinksListCtrl'
            }).
            when('/links/:linkId', {
                templateUrl: 'templates/links-detail.html',
                controller: 'LinkDetailCtrl'
            }).
            otherwise({
                redirectTo: '/links'
            });
    }])
