"use strict";

angular.module('mishabot.controllers', []).
    controller('BannerSelectCtrl', ['$scope', '$location', 'Settings',
        function ($scope, $location, Settings) {
            // Easter egg. Try /?sayaka=true
            if ('sayaka' in $location.search())
                Settings.banner = Settings.banners.sayaka;

            var banner = Settings.banner;
            if (!banner)
                banner = { file: 'default.png' };
            $scope.banner = banner;
        }]
    ).
    controller('LinksListCtrl', ['$scope', 'Link', function($scope, Link) {
        $scope.links = Link.enumerate();
    }]).
    controller('LinkDetailCtrl', [function() {
        
    }]);

