"use strict";

angular.module('mishabot.directives', []).
    directive('option', function () {
        return {
            replace: true,
            restrict: 'C',
            transclude: true,
            scope: { title: '@title' },
            template: '<div> <div class="title">{{title}}</div>' +
                            '<div class="body" ng-transclude></div>' +
                      '</div>',
            link: function (scope, element, attrs) {
                // jqLite doesn't provide .on()?
                var title = angular.element(element.children()[0]),
                    body = angular.element(element.children()[1]),
                    open = false;

                function toggle() {
                    body.slideToggle();
                    open = !open;
                    element.removeClass(open ? 'closed' : 'open');
                    element.addClass(open ? 'open' : 'closed');
                }
                element.addClass('closed');
                title.on('click', toggle);
            }
        };
    });
