"use strict";

angular.module('mishabot.services', ['ngResource', 'ngCookies']).
    factory('Link', function ($resource) {
        return $resource('links/:id', {}, {
            enumerate: {
                method: 'GET',
                isArray: true
            }
        });
    }).
    factory('Settings', function ($cookieStore) {
        var BANNERS = [
            { name: 'Misha', file: 'default.png' }
        ];
        // Easter egg
        BANNERS.sayaka = { name: 'Sayaka', file: 'sayaka.jpg' };

        // TODO must actually set defaults
        if ($cookieStore.get('settings') === undefined) {
            console.log('no settings');
            $cookieStore.put('settings', {});
        }
        // Defaults
        var currentSettings = {
            banner: BANNERS[0],
        };
        // Load from cookie
        angular.extend(currentSettings, $cookieStore.get('settings'));

        return angular.extend({
            banners: BANNERS,

            // TODO
            commit: function () {
                // $cookieStore.put(...)
            },
            revert: function () {

            }
        }, currentSettings);
    });
