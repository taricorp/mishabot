from datetime import datetime
import json
import os.path
import calendar, time

from twisted.internet.threads import deferToThread
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.static import File
from twisted.web.resource import ErrorPage, NoResource, Resource

from mishabot import db
from mishabot.db import Session, URL

# TODO use pkg_resources
webdir = os.path.join(os.path.dirname(__file__), 'web')

def _cb_render_with_db(result, request):
    if not request.finished and result != NOT_DONE_YET:
        request.write(result)
        request.finish()
    return result
def _eb_render_with_db(err, request):
    request.processingFailed(err)
def render_with_db(f):
    """Decorator to run a function with a database session.

    sqlalchemy methods may block while accessing the database, so this
    arranges to execute the function in a non-reactor thread, preserving
    the render() semantics of IResource.

    Signature of the decorated function should be
    >>> f(self, session, request, *args, **kwargs)
    """
    # Callback to write returned literals
    # Defer f to a thread, but act like it's not in a thread
    @db.withSession
    def g(self, session, request, *args, **kwargs):
        d = deferToThread(f, self, session, request, *args, **kwargs)
        d.addCallback(_cb_render_with_db, request)
        d.addErrback(_eb_render_with_db, request)
        return NOT_DONE_YET
    return g

def render_ranged(f):
    """Range-decoded render wrapper.

    Uses the 'start' and 'count' URL parameters to generate a slice object
    for the worker,
    >>> f(self, request, slice, *args, **kwargs)

    Count is clamped to a maximum of 100 items and a minimum of 1.
    """
    def g(self, request, *args, **kwargs):
        start = 0
        count = 100
        try:
            if 'start' in request.args:
                start = int(request.args['start'][0])
            if 'count' in request.args:
                count = min(count, int(request.args['count'][0]))
        except ValueError as e:
            request.setResponseCode(400)
            return e.message
        stop = start + max(count, 1)

        range = slice(start, stop)
        return f(self, request, range, *args, **kwargs)
    return g

def db_ranged(f):
    """Shortcut for DB methods with range parameters.
    
    Resultant signature is
    >>> f(self, session, request, slice, *args, **kwargs)
    """
    return render_ranged(render_with_db(f))

class URLEncoder(json.JSONEncoder):
    PROPS = {
        'id': None,
        'url': None,
        'sfw': None,
        'title': None,
        'timestamp': lambda t: calendar.timegm(t.utctimetuple()),
        'network': None,
        'channel': None,
        'nick': None
    }
    def __init__(self, *args, **kwargs):
        if 'props_filter' in kwargs:
            self.props_filter = kwargs['props_filter']
            del kwargs['props_filter']
        else:
            self.props_filter = None
        super(URLEncoder, self).__init__(*args, **kwargs)

    def default(self, obj):
        if isinstance(obj, URL):
            if self.props_filter:
                props = filter(lambda k: k in self.props_filter, self.PROPS.keys())
            else:
                props = self.PROPS.iterkeys()

            out = {}
            for k in props:
                f = self.PROPS[k] or (lambda x: x)
                out[k] = f(getattr(obj, k))
            return out
        return json.JSONEncoder.default(self, obj)

class LinksById(Resource):
    """Dispatcher for /link/id/:id:"""
    def getChild(self, name, request):
        try:
            return self.LinkById(int(name))
        except ValueError:
            return ErrorPage(400, "Invalid link ID", name)

    class LinkById(Resource):
        """Endpoint for /link/id/:id:"""
        def __init__(self, id):
            self.id = id

        @render_with_db
        def render_GET(self, session, request):
            request.setHeader('Content-Type', 'application/json')
            l = session.query(URL).get(self.id)
            if l:
                return json.dumps(l, cls=URLEncoder)
            else:
                request.setResponseCode(404)
                return b"\"No such link. (id = {0})\"".format(self.id)

class LinksNewerThan(Resource):
    """Dispatcher for /link/newer/:id:"""
    def getChild(self, name, request):
        try:
            return self.NewerEndpoint(int(name))
        except ValueError:
            return ErrorPage(400, "Invalid link ID", name)

    class NewerEndpoint(Resource):
        def __init__(self, id):
            self.id = id

        @db_ranged
        def render_GET(self, session, request, slice):
            request.setHeader('Content-Type', 'application/json')
            base = session.query(URL).get(self.id)
            if base is None:
                request.setResponseCode(404)
                return b"\"No such link. (id = {0})\"".format(self.id)

            # Secondary ordering on ID to ensure stacking is consistent,
            # important because we only get second resolution on timestamps.
            # >= test means we must specifically exclude the base point.
            l = session.query(URL) \
                    .filter(URL.timestamp >= base.timestamp) \
                    .filter(URL.id != base.id) \
                    .order_by(URL.timestamp, URL.id)
            return json.dumps(l[slice.start:slice.stop], cls=URLEncoder)

class Links(Resource):
    """Dispatcher for /link/...
    
    Top-level resource is a ranged endpoint in whatever order the db gives back."""
    def __init__(self):
        Resource.__init__(self)
        # render this with or without a trailing slash
        self.putChild('', self)
        self.putChild('id', LinksById())
        self.putChild('newer', LinksNewerThan())

    @db_ranged
    def render_GET(self, session, request, range):
        request.setHeader('Content-Type', 'application/json')
        links = session.query(URL)[range.start:range.stop]
        return json.dumps(links, cls=URLEncoder)

class ContextResource(Resource):
    """Some kind of thing allowing users to get context for links (single or
    multiline logs)."""
    pass

def getSite():
    root = File(webdir)
    root.putChild('links', Links())
    return Site(root)
