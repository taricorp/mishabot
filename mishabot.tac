
from twisted.application import service, internet

from mishabot import irc, http

servers = ['irc.prison.net']
channels = ['#flood']

# TODO subclass TCPClient to handle multiple servers
def getIRCService():
    factory = irc.MishaBotNetwork(servers, channels)
    return internet.TCPClient(servers[0], 6667, factory)

def getWebService():
    return internet.TCPServer(8000, http.getSite())

application = service.Application('MishaBot')
irc = getIRCService()
irc.setServiceParent(application)
web = getWebService()
web.setServiceParent(application)

