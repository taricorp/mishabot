from setuptools import setup

setup(name='Mishabot',
      version='0.2',
      description='Link-logging IRC bot and web interface',
      author='Peter Marheine',
      author_email='peter@taricorp.net',
      url='http://mishabot.taricorp.net/',
      packages=['mishabot'],
      requires=['twisted', 'sqlalchemy']
     )
